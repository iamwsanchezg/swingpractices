/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
*/
package misc;

import java.math.BigDecimal;

public class Math {
    //Devuelve el n término de la serie Fibonacci
    //La sucesión comienza con los números 0 y 1, y a partir de estos, «cada término es la suma de los dos anteriores»
    public static double getFibonacci(int n) {
        double s=1,u=1,p;
        int i;
        switch (n) {
            case 0:
                s=0;
                break;
            case 1:
            case 2:
                s=1;
                break;
            default:
                for(i=3;i<=n;i++)
                {
                    p=u;
                    u=s;
                    s=s+p;
                }
                break;
        }
        return s;
    }
    //Devuelve la serie Fibonacci hasta el número n
    //La sucesión comienza con los números 0 y 1, y a partir de estos, «cada término es la suma de los dos anteriores»
    public static String getFibonacciSeries(int n) {
        double s=1,u=1,p;
        String series="";
        int i;
        switch(n){
            case 0:
                series="0";
                break;
            case 1:
                series="0, 1";
                break;
            case 2:
                series="0, 1, 1";
                break; 
            default:
                series="0, 1, 1";
                 for(i=3;i<=n;i++)
                {
                    p=u;
                    u=s;
                    s=s+p;
                    series+=", "  + String.valueOf(s);
                }               
                break;
        }
        return series;
    }	    
    //Devuel el factorial de un entero positivo
    //el factorial de n o n factorial se define en principio como el producto de todos los números enteros positivos desde 1 hasta n 
    public static double getFactorial(int n){
        int i;
        double f=1;
        if (n>1)
            for (i=2;i<=n;i++)
                f*=i;
        return f;
    }    
    //En matemáticas, un número primo es un número natural mayor que 1 que tiene únicamente dos divisores distintos: él mismo y el 1.
    public static boolean isPrimeNumber(int n) {
        int c=0,i=1;
        if (n>2)
          while ((i<=n)&&(c<3)){
               if((n%i)==0)
                   c++;
               i++;
           }
       return !(c>2);
    }    
    public static boolean isNumeric(String s) {  
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
    }      
    public static String getNumberToString(Double number){
        BigDecimal d = new BigDecimal(number);
        long a = d.longValue();
        BigDecimal b = d.remainder(BigDecimal.ONE);
        if (b==BigDecimal.ZERO) {
            return String.valueOf(a);
        } else {
            return String.valueOf(number);
        }       
    }
}
