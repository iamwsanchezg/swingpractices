/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package misc;

import javax.swing.LookAndFeel;

/**
 *
 * @author William Sanchez
 */
public class SupportedLaF {

        private final String name;
        private final LookAndFeel laf;

        public SupportedLaF(String name, LookAndFeel laf) {
            this.name = name;
            this.laf = laf;
        }

        @Override
        public String toString() {
            return name;
        }
        
        public LookAndFeel getLookAndFeel(){
            return laf;
        }
}
