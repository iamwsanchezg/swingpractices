/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package views;

import controllers.BookController;
import javax.swing.table.TableModel;
import models.FileTableModel;

/**
 *
 * @author William Sanchez
 */
public class BookList extends javax.swing.JInternalFrame {
    BookController bc;
    /**
     * Creates new form BookList
     */
    public BookList() {
        initComponents();
        booksTable.setModel(new FileTableModel().getModel());
        setController();
    }
    private void setController(){
        bc = new BookController(this);
        openDirectoryButton.addActionListener(bc);
        newBookButton.addActionListener(bc);
        showBookButton.addActionListener(bc);
    }
    public void setBookTableModel(TableModel model){
        booksTable.setModel(model);
    }
    public void setDirectory(String path){
        directoryTextField.setText(path);
    }
    public String getDirectory(){
        return directoryTextField.getText();
    }
    public String getSelectedBookFile(){
        return (String) booksTable.getValueAt(booksTable.getSelectedRow(), 1);
    }    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tb = new javax.swing.JToolBar();
        openDirectoryButton = new javax.swing.JButton();
        newBookButton = new javax.swing.JButton();
        showBookButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        directoryLabel = new javax.swing.JLabel();
        directoryTextField = new javax.swing.JTextField();
        booksScrollPane = new javax.swing.JScrollPane();
        booksTable = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/table.png"))); // NOI18N

        tb.setRollover(true);

        openDirectoryButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/open.png"))); // NOI18N
        openDirectoryButton.setText("Directorio");
        openDirectoryButton.setToolTipText("");
        openDirectoryButton.setActionCommand("directory");
        openDirectoryButton.setFocusable(false);
        openDirectoryButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        openDirectoryButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tb.add(openDirectoryButton);

        newBookButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/new.png"))); // NOI18N
        newBookButton.setText("Nuevo");
        newBookButton.setActionCommand("new");
        newBookButton.setFocusable(false);
        newBookButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        newBookButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tb.add(newBookButton);

        showBookButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/address-book.png"))); // NOI18N
        showBookButton.setText("Ver");
        showBookButton.setActionCommand("show");
        showBookButton.setFocusable(false);
        showBookButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showBookButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tb.add(showBookButton);

        closeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/close.png"))); // NOI18N
        closeButton.setText("Cerrar");
        closeButton.setFocusable(false);
        closeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        closeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        tb.add(closeButton);

        directoryLabel.setText("Directorio");
        tb.add(directoryLabel);

        directoryTextField.setEnabled(false);
        directoryTextField.setMinimumSize(new java.awt.Dimension(500, 24));
        directoryTextField.setPreferredSize(new java.awt.Dimension(500, 24));
        tb.add(directoryTextField);

        getContentPane().add(tb, java.awt.BorderLayout.NORTH);

        booksScrollPane.setViewportView(booksTable);

        getContentPane().add(booksScrollPane, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        dispose();
    }//GEN-LAST:event_closeButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane booksScrollPane;
    private javax.swing.JTable booksTable;
    private javax.swing.JButton closeButton;
    private javax.swing.JLabel directoryLabel;
    private javax.swing.JTextField directoryTextField;
    private javax.swing.JButton newBookButton;
    private javax.swing.JButton openDirectoryButton;
    private javax.swing.JButton showBookButton;
    private javax.swing.JToolBar tb;
    // End of variables declaration//GEN-END:variables
}
